# Script to Update IP against Afraid's FreeDNS service. Example is for 30 minutes.  
# If created as a systemd sevice, the systemd service will automatically restart itself everytime the counter reaches zero.
import subprocess
import time


def DNSUpdate():
        target = 'https://freedns.afraid.org/dynamic/update.php?[INSERT-YOUR-HASH-HERE]'
        command = 'curl {}' .format(target)
        atualizar = subprocess.call(command)
        return

def Tempo():
        time.sleep(1800)
        DNSUpdate()


if __name__ == '__main__':
        while True:
                Tempo()
